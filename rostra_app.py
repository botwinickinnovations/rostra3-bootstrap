# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
if __name__ == '__main__':
    try:
        import bootstrap

        environment = bootstrap.initialize_environment()

        # custom settings
        environment.app_id = 'rostra3.platform'
        environment.app_title = 'Rostra 3 Application'
        environment.company_name = 'Botwinick Innovations'

        exec "bootstrap.start(environment)"  # use exec to force a parent stack frame (cython compiled code workaround)
    except ImportError as e:
        bootstrap = None
        ASSETS_PATH = None
        print('FATAL ERROR: Unable to finish initializing bootstrap mechanism.')
