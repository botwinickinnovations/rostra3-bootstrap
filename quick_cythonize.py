import sys
from distutils import sysconfig
from os import path as osp, putenv
from subprocess import check_call as _call

PYTHON = sys.executable
PY_VERSION = '%s.%s' % (sys.version_info.major, sys.version_info.minor)
INC_DIR = sysconfig.get_python_inc()
PLAT_INC_DIR = sysconfig.get_python_inc(plat_specific=True)
LIB_DIR1 = sysconfig.get_config_var('LIBDIR')
LIB_DIR2 = sysconfig.get_config_var('LIBPL')
PY_LIB = sysconfig.get_config_var('LIBRARY')[3:-2]  # library removing 'lib' and '.so'

CC = sysconfig.get_config_var('CC')
LINK_CC = sysconfig.get_config_var('LINKCC')
LINK_FOR_SHARED = sysconfig.get_config_var('LINKFORSHARED')
LIBS = sysconfig.get_config_var('LIBS')
SYS_LIBS = sysconfig.get_config_var('SYSLIBS')


def cythonize(py_file, embed=True):
    from Cython.Compiler.Main import compile, Options
    if embed:
        Options.embed = 'main'
    return str(compile(osp.abspath(py_file)).c_file)


def cc(c_file, library=True):
    args = CC.split() + [
        '-c', '-I%s' % INC_DIR, '-I%s' % PLAT_INC_DIR, c_file
    ]
    if library:
        args.extend(['-fPIC', '-O3'])
    else:
        args.extend(['-Os'])
    shelled = ' '.join(args)
    # print(shelled)
    _call(shelled, shell=True)
    return '%s.o' % (osp.splitext(c_file)[0])


def link(o_file, library=False):
    target = str(osp.splitext(o_file)[0])
    if library:
        target += '.so'
    args = LINK_CC.split() + [
        '-o', target, o_file,
        '-L%s' % LIB_DIR1, '-L%s' % LIB_DIR2, '-l%s' % PY_LIB,
        LIBS, SYS_LIBS, LINK_FOR_SHARED
    ]
    if library:
        args.append('-shared')
    shelled = ' '.join(args)
    # print(shelled)
    _call(shelled, shell=True)
    return target


embed = False
args = sys.argv[1:]
if args[0] == 'embed':
    embed = True
    args.pop(0)

RPATHS = ':'.join(args[1:])
putenv('LD_RUN_PATH', RPATHS)  # default: '../dependencies/lib'
print('RPATHS: "%s"' % RPATHS)

print('Generating embeddable c source')
c_file = cythonize(args[0], embed=embed)

print('Compiling')
o_file = cc(c_file)

print('Linking')
exe = link(o_file, library=not embed)

print('Done.')

pass
# BASIS FOR CODE IS FROM Makefile from cython git Demos/embed
# # Makefile for creating our standalone Cython program
# PYTHON := python
# PYVERSION := $(shell $(PYTHON) -c "import sys; print(sys.version[:3])")
#
# INCDIR := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_python_inc())")
# PLATINCDIR := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_python_inc(plat_specific=True))")
# LIBDIR1 := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBDIR'))")
# LIBDIR2 := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBPL'))")
# PYLIB := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBRARY')[3:-2])")
#
# CC := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('CC'))")
# LINKCC := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LINKCC'))")
# LINKFORSHARED := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LINKFORSHARED'))")
# LIBS := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LIBS'))")
# SYSLIBS :=  $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('SYSLIBS'))")
#
# embedded: embedded.o
# 	$(LINKCC) -o $@ $^ -L$(LIBDIR1) -L$(LIBDIR2) -l$(PYLIB) $(LIBS) $(SYSLIBS) $(LINKFORSHARED)
#
# embedded.o: embedded.c
# 	$(CC) -c $^ -I$(INCDIR) -I$(PLATINCDIR)
#
# CYTHON := ../../cython.py
# embedded.c: embedded.pyx
# 	@$(PYTHON) $(CYTHON) --embed embedded.pyx
#
# all: embedded
#
# clean:
# 	@echo Cleaning Demos/embed
# 	@rm -f *~ *.o *.so core core.* *.c embedded test.output
#
# test: clean all
# 	LD_LIBRARY_PATH=$(LIBDIR1):$$LD_LIBRARY_PATH ./embedded > test.output
# 	$(PYTHON) assert_equal.py embedded.output test.output
