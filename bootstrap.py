# encoding: utf-8
# cython: embedsignature=False, linetrace=False, profile=False, emit_code_comments=False
__compiler__ = 'cython'

# bootstrap script

import sys
from logging import getLevelName as _translateLogLevelName
from os import getenv, path as osp

_DEBUG_FRAME_FILE = False


def _sys_paths(frame_offset=0):
    # frame offset is basically the number of stack frames deep this function is being called from...
    import inspect
    frame = None
    try:
        # try to fetch a frame for investigating paths
        try:
            frame = inspect.stack()[1 + frame_offset]
        except (ValueError, IndexError):
            if _DEBUG_FRAME_FILE:
                print('DEBUG: Unable to get specified frame; will continue without frame information')
            pass

        # try to use the filename from a frame up the stack to isolate our bin directory
        if frame and '__file__' in frame[0].f_locals:  # try __file__ variable in frame locals
            f__file__ = frame[0].f_locals['__file__']
            if _DEBUG_FRAME_FILE:
                print('DEBUG: Using upstream frame local __file__ variable to determine bin directory')
        elif frame and osp.isfile(frame[1]):  # try filename from interpreter in frame
            f__file__ = frame[1]
            if _DEBUG_FRAME_FILE:
                print('DEBUG: Using upstream frame interpreted filename to determine bin directory')
        # TODO: should we have additional attempts before falling back to the sys.executable?
        else:  # __file__ / filename is not defined for embedded python interpreters; use sys.executable
            f__file__ = sys.executable
            if _DEBUG_FRAME_FILE:
                print('DEBUG: Using sys.executable to determine bin directory')

        # use whatever we've determined to come up with our base bin and plugins paths
        bin_dir = osp.abspath(osp.dirname(osp.realpath(f__file__)))
        plugins_dir = osp.normpath(osp.join(bin_dir, osp.pardir, 'plugins'))
    finally:
        del frame  # ensure that frame reference is removed
    return bin_dir, plugins_dir


class ExecutionEnvironment(object):
    core_logger_name = None
    log_level = _translateLogLevelName('INFO')
    suppress_welcome = False  # option to suppress the welcome messages on the logger

    bin_dir = None
    main_plugins_dir = None
    additional_plugins_dirs = None
    plugins_match_include = None  # list of glob-like strings of plugin ids that are allowed
    plugins_match_exclude = None  # list of glob-like strings of plugin ids that are disallowed

    app_title = None  # modify the title of the core application
    company_name = None  # option to set company name used for ETSConfig - preferences directories etc.
    force_cli = False  # option disable GUI / force CLI ???
    app_id = None  # option to set app ID

    main_assets_dir = None  # override for assets directory
    additional_assets_dirs = None  # additional assets directories

    # TODO: implement the perspective management stuff...
    disable_perspectives_ui = False  # disable user interface component(s) of perspectives [e.g. perspectives menu]
    default_perspective = None  # set default/initial perspective shown in workbench windows

    args = None  # CLI args or remainder args if parsed by bootstrap

    pass


def _str_upper(s):
    return str(s).upper()


def _std_parser():
    """ Define a standard parser for Rostra application ExecutionEnvironment configuration """
    from argparse import ArgumentParser
    parser = ArgumentParser(
        description=""" This describes the arguments common to all Rostra applications.
            Specific Rostra applications may include their own additional arguments. """
    )

    # CORE OPTIONS
    core = parser.add_argument_group('Core')
    core.add_argument('--force-cli', action='store_true', dest='force_cli',
                      help='Force loading without initializing the GUI and skipping all UI plugins')

    # PLUGIN OPTIONS
    plugins = parser.add_argument_group('Plugins Management')
    plugins.add_argument('--plugin-path', action='append', dest='plugin_paths', metavar='DIR',
                         help='Add an additional plugins path. This option can be used multiple times.')

    # LOGGING OPTIONS
    logging = parser.add_argument_group('Logging')
    logging.add_argument('--log-level', type=_str_upper, default='INFO', dest='log_level',
                         choices=('INFO', 'WARNING', 'ERROR', 'CRITICAL', 'DEBUG'),
                         help='Define the logging level to be used by the application')
    logging.add_argument('--suppress-welcome', action='store_true', dest='suppress_welcome',
                         help="Suppress 'welcome' logging messages when starting the application.")
    # logging.add_argument('--override-title', type=str, dest='welcome_name', metavar='NAME',
    #                      help='Define an alternative title to be used for the logging welcome and core.')

    return parser


def _parse_cli_args(argv, env):
    """ :type env: ExecutionEnvironment """
    options, env.args = _std_parser().parse_known_args(argv)

    # ########################## #
    # SET OPTIONS IN ENVIRONMENT #
    # ########################## #

    # either set or extend additional plugins in the execution environment
    if not env.additional_plugins_dirs:
        env.additional_plugins_dirs = options.plugin_paths
    elif options.plugin_paths:
        # TODO: consider if the CLI plugin paths should come before or after any existing additional plugin paths
        env.additional_plugins_dirs.extend(options.plugin_paths)

    # translate the text logging level to the python logging module integer
    env.log_level = _translateLogLevelName(options.log_level)

    # here is a section for simple pass-through assignments that feel like a waste of time...
    # (i.e.... should've used environment as the namespace object and just fixed the exceptional cases...)
    env.suppress_welcome = options.suppress_welcome
    env.force_cli = options.force_cli
    # env.app_title = options.welcome_name
    return env  # not really needed... but whatever, gives options?


def _parse_environment_vars(exec_env):  # type: ExecutionEnvironment
    log_level = getenv('ROSTRA_LOG_LEVEL')
    if isinstance(log_level, str):
        log_level = _translateLogLevelName(log_level.upper())
        if isinstance(log_level, int):  # check if it has been translated to an integer
            exec_env.log_level = log_level

    exec_env.force_cli = bool(getenv('ROSTRA_FORCE_CLI', False)) or bool(getenv('ROSTRA_DISABLE_GUI', False))

    # TODO: support other environment variables
    pass


def initialize_environment(args=None):
    # frame offset of zero means determine from here; works IF bootstrap library is placed in bin directory!
    base_paths = _sys_paths(frame_offset=-1)
    for path in base_paths:
        if path not in sys.path:
            sys.path.append(path)

    env = ExecutionEnvironment()
    env.bin_dir = base_paths[0]  # use index access to support future extension, but not really necessary...
    env.main_plugins_dir = base_paths[1]
    env.executable = sys.argv[0]

    _parse_environment_vars(env)
    _parse_cli_args(args if args is not None else sys.argv[1:], env)
    return env


def start(environment, app_id=None):
    """
    :type environment: ExecutionEnvironment
    :type app_id: str
    """
    import rostra3

    if app_id:
        rostra3.ID = app_id
    elif environment.app_id:  # if one isn't provided at this fn but is provided in the environment, use that...
        rostra3.ID = environment.app_id
    rostra3.R3_BIN_DIR = environment.bin_dir
    rostra3.R3_PLUGINS_DIR = environment.main_plugins_dir

    if environment.core_logger_name:
        rostra3.CORE_LOGGER_NAME = environment.core_logger_name

    from rostra3.core import bootstrap as inner_start
    env_data = dict((name, getattr(environment, name)) for name in dir(environment) if not name.startswith('__'))

    m = inner_start(env_data)
    return m
